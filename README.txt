README.txt

:Author: Nikola Prica
:Email: nikola.prica@rt-rk.com
:Date: 2017-12-19 20:40

Sledece komande skidaju llvm, clang i compiler-rt, čekiraju se na zvaničnu verziju 3.8 i primenjuju peč za prosirenje mapirajuceg pokrivaca kompajlera clang kao i peč za obezbeđivanje jednog funkcijskog poziva za ispisivanje podataka profajliranja.


#Skidanje LLVM projekta i pozicioniranje na verzijue 3.8
git clone http://llvm.org/git/llvm.git llvm/
cd llvm/
git checkout release_38
#Skidanje Clang projekta i pozicioniranje na verziju 3.8
cd tools/
git clone http://llvm.org/git/clang.git
cd calng/
git checkout release_38
#Skidanje compiler-rt projekta i pozicioniranje na verziju 3.8
cd ../../projects/
git clone http://llvm.org/git/compiler-rt.git
cd compiler-rt/
git checkout release_38
#Primena peča za proširivanje mapirajućeg pokrivača
cd ../../
patch -p1 0001-Branch-coverage-and-llvm-cov-geninfo.patch
#Primena peča koji implementira jedan funkcijski poziv za ispisivanje podataka profajliranja
patch -p1 0002-Dump-profile-data-with-one-function.patch
#Konfiguracija sistema i pokretanje izgranje LLVM projekta
mkdir build
cd build
cmake -G "Unix Makefiles" ../
make -j4
