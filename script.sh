#!/usr/bin

#Skidanje LLVM projekta i pozicioniranje na verzijue 3.8
git clone http://llvm.org/git/llvm.git llvm/
cd llvm/
git checkout release_38
#Skidanje Clang projekta i pozicioniranje na verziju 3.8
cd tools/
git clone http://llvm.org/git/clang.git
cd calng/
git checkout release_38
#Skidanje compiler-rt projekta i pozicioniranje na verziju 3.8
cd ../../projects/
git clone http://llvm.org/git/compiler-rt.git
cd compiler-rt/
git checkout release_38
#Primena peča za proširivanje mapirajućeg pokrivača
cd ../../
patch -p1 0001-Branch-coverage-and-llvm-cov-geninfo.patch
#Primena peča koji implementira jedan funkcijski poziv za ispisivanje podataka profajliranja
patch -p1 0002-Dump-profile-data-with-one-function.patch
#Konfiguracija i pokretanje izgranje sistema
mkdir build
cd build
cmake -G "Unix Makefiles" ../
make -j4
